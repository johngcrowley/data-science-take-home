import pandas as pd
import re



addys = pd.read_csv('dirty_addresses.csv',error_bad_lines=False)
cities = pd.read_csv('texas_cities.csv')

def addys_to_csv():
    # regex into dictionary format to place extractions right into columns. 
    # running out of time. can't do apartments. 
    patt = re.compile('^(?P<number>\d{1,10})\s'
                        '(?P<words>[A-Za-z\s]*)\s'
                        '(?P<zip>\d+[-]?\d+)$')
    s = addys['address']
    df = s.str.extractall(patt)

    #logic: in the words column,
        #-- problem: what about 2-word cities, can't just lop-off last one
        #-- solution: compile list of street-esque words by lopping-off last one, make a set.

    # either these will be street names or the prefix of a two-part city name. only 39 of them, easy to check through.
    unique_end_words = set(df.words.str.lower().str.split().str[-2])

    # turn those unique street abbreviations / variations into a list
    road_abbrevs = ['road','rd','court','bend','ct','way','trl','pl','pkwy','lane','ln','trail','drive','dr','crossing','circle','cir','blvd','avenue','ave','st']

    # do similarly with two-part city names considering city names in texas
    city_prefixes = ['san','saint','la','fort']

    #instantiate these to None so .iloc doesn't throw us a key error
    df['city'] = None
    df['street'] = None

    # check each word in each column against road abbreviations
    # then check against city prefixes
    # i.e. if it's San Antonio, "rd" won't matter, so put that condition 2nd
    for i,row in enumerate(df.words.str.lower().str.split()):
        for j,word in enumerate(row):
            if word in road_abbrevs:
                df['street'].iloc[i] = ' '.join(row[:j+1]).title()
                df['city'].iloc[i] = ' '.join(row[j+1:]).title()
            if word in city_prefixes:
                df['street'].iloc[i] = ' '.join(row[0:j]).title()
                df['city'].iloc[i] = ' '.join(row[j:]).title()


    #--------Output--------------------------
    didnt_match = df[df.isnull().any(axis=1)] 
    missed_overall = abs(len(df) - len(addys))
    print(f'our logic didn\'t match on {len(didnt_match)} records\n')
    print(f'running out of time.. \nthe regex (due to not factoring for apartment numbers) didn\'t match {missed_overall} records')

    # drop uneeded columns before output to CSV
    df = df.reset_index().drop(columns=['match','words','level_0'])

    # export to file tree / repo
    return df.to_csv('part_one_output.csv')

# ------- run it ----------
addys_to_csv()